# Segmentation and Track-Analysis in Time-Lapse Imaging of Bacteria #

In this paper, we have developed tools to analyze prokaryotic cells growing in monolayers in a microfluidic device. Individual bacterial cells are identified using a novel curvature based approach and tracked over time for several generations. The resulting tracks are thereafter assessed and filtered based on track quality for subsequent analysis of bacterial growth rates. The proposed method performs comparable to the state-of-the-art methods for segmenting phase contrast and fluorescent images, and we show a 10-fold increase in analysis speed.


### Installation steps ###

###Step1###
Dependencies to be installed

* python
* opencv for python
* scikit-image
* scikit-learn
* numpy
* scipy
* matplotlib

### Step2###
* Uncomment/comment lines 61-64 depending on dataset. These are parameters specific to each image.

###Step3###
* Uncomment/comment lines 111-115 depending on the file to read. 

###Step4###
* For fluorescent data uncomment line 118 (for inverting the image)

###Step5###
* To run the code type at the command prompt

>>python cba.py


If you need further help please ask.
sajith.ks@it.uu.se